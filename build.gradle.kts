import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


buildscript {
    repositories {
        jcenter()
        mavenCentral()
    }
}

plugins {
    kotlin("jvm") version "1.4.10"
    java
    maven
    id("signing")
    id("org.jetbrains.dokka") version "1.4.10.2"
    id("hu.vissy.semantic-versioning") version "1.0.3"
}

group = "hu.vissy.plain-text-table"
//version = "1.0.0"

semanticVersion {
    releaseNotesFile = "changelog.md"
}


repositories {
    mavenCentral()
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation("hu.vissy.plain-text-table:ptt-core:3.0.0")

    testImplementation("org.testng:testng:7.0.0")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}




@Suppress("UnstableApiUsage")
signing {
    useGpgCmd()
    sign(configurations.archives.get())
}

tasks {

    withType<Test> { useTestNG() }

    withType<JavaCompile> {
        options.encoding = "UTF-8"
        sourceCompatibility = "11"
    }

    withType<KotlinCompile> {
        dependsOn.add(updateVersion)
        kotlinOptions.jvmTarget = "11"
    }

    val sourcesJar by creating(Jar::class) {
        dependsOn.add(updateVersion)
        archiveClassifier.set("sources")
        from(sourceSets.main.get().allSource)
    }

    val javadocJar by creating(Jar::class) {
        dependsOn.add(dokkaJavadoc)
        dependsOn.add(updateVersion)
        archiveClassifier.set("javadoc")
        from(dokkaJavadoc)
    }

    artifacts {
        archives(sourcesJar)
        archives(javadocJar)
        archives(jar)
    }

    named<Upload>("uploadArchives") {
        dependsOn("updateVersion")
        outputs.upToDateWhen { !semanticVersion.hasNewVersion }

        finalizedBy(closeVersion)

        val releasesRepoUrl = "https://oss.sonatype.org/service/local/staging/deploy/maven2/"
        val snapshotsRepoUrl = "https://oss.sonatype.org/service/local/staging/deploy/snapshots/"
        val ossrhUser = project.ext.properties["ossrhUser"].toString() //: String by project
        val ossrhPassword = project.ext.properties["ossrhPassword"].toString()

        repositories {

            withConvention(MavenRepositoryHandlerConvention::class) {
                mavenDeployer {
                    beforeDeployment {
                        signing.signPom(this)
                    }

                    withGroovyBuilder {
                        "repository"("url" to releasesRepoUrl) {
                            "authentication"("userName" to ossrhUser, "password" to ossrhPassword)
                        }
                        "snapshotRepository"("url" to snapshotsRepoUrl) {
                            "authentication"("userName" to ossrhUser, "password" to ossrhPassword)
                        }
                    }

                    pom.project {
                        withGroovyBuilder {
                            "groupId"("hu.vissy.plain-text-table")
                            "artifactId"("ptt-kotlin")
                            "version"(semanticVersion.state.newVersion.versionSequence)
                            "name"("ptt-kotlin")
                            "description"("Kotlin DSL for plain-text-table, a customizable plain-text based table generator")
                            "url"("https://bitbucket.org/balage42/ptt-kotlin")
                            "licenses" {
                                "license" {
                                    "name"("The Apache Software License, Version 2.0")
                                    "url"("http://www.apache.org/licenses/LICENSE-2.0.txt")
                                }
                            }

                            "developers" {
                                "developer" {
                                    "id"("Balage1551")
                                    "name"("Balazs Vissy")
                                    "email"("balage42-maven@yahoo.com")
                                }
                            }
                            "scm" {
                                "connection"("scm:git:git://bitbucket.org/balage42/ptt-kotlin.git")
                                "developerConnection"("scm:git:ssh://bitbucket.org/balage42/ptt-kotlin.git")
                                "url"("https://bitbucket.org/balage42/ptt-kotlin/src/master/")
                            }
                        }
                    }
                }
            }
        }
    }
}