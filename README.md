# Plain-text Table Formatter Kotlin DSL Guide

## Introduction

Have you ever been lost finding data in a log file, when it was logged as plain text? Have you ever 
been trouble to look over a list of objects in a log file? Have you ever spent long hours to format 
tabular information in text files for better readability?

The *Plan-text table formatter* offers a fluent way to format and print your data as a table in text.
The main goal of the library to offer a declarative way to describe the structure of the table, while
give the freedom of printing any business object.

## What PTT is?

A lightweight and highly flexible and extensible library to configure plain text table. 

**Main functional features:**

- **Direct user object to table conversion:** no wrapping or intermediate classes are required. Any valid 
    Java or Kotlin class is accepted.

- **Separated configuration and processing:** easy reuse of configuration elements.

- **Separated conversion steps:** the formatting is done in separated and independent steps. Each step has 
    a well-defined scope of responsibility, keeping them simple and interoperable.
    
- **Sensible default values for easy prototyping:** use defaults for quick output then fine tune it later.

- **Stateful columns and aggregation:** columns may maintain state information and provide aggregated 
    values (such as count, sum, avg, or moving window calculations).

- **Different layout presets:** you can pick from one of the out-of-box layouts and use it as is, or tweak 
    it for your needs.
   
- **CSV export support:** out-of-box support for csv export.

**Main architectural features:**

- **Declarative DSL**: declarative definition based on Kotlin internal DSL architecture
 
- **Modular configuration**: splits the conversion process into steps, which makes it easy to add your 
  implementation to the point you wish without much boiler code.
  
- Most of the configuration classes are **imutables** for wide and safe reusability.

- **Ultra lightweight:** the main jar is less than 100k, no external library requirements.


## The concept behind

The concept behind the design of the library is to allow a fluent way to present any 
table-like data in plain text. To achieve flexibility while keeping simplicity, 
the formatting process is divided into several steps:

1. **Data extract**: the cell value is extracted from the input record (both the source
   object and the extracted data may be any Java or Kotlin class). This is the only step 
   depends on your business data structure.
   
2. **Data conversion**: Then convert the extracted value to a string while applying type 
   specific formatting on it.
   
3. **Cell content formatting**: Decorate the converted value to match the column specification.

4. **Table build:** build up the table from the cell contents and the border rules.

These four steps are independent and may be configured independently. For example, you may use 
the default formatting of your numeric data, but apply special cell content formatting.

For quick prototyping most of the steps has their sensible defaults.

## Getting started

The following two examples only scratches the surface of the possibilities of the library, but lets
an insight of the overall structure of the DSL.

Both examples use the following business object:

```kotlin
data class TutorialData(val name: String, val quantity: Double)
```

First, let's see a very simple one.

```kotlin
val formatter = tableFormatter<TutorialData> {
    // A simple column 
    simple("Name", TutorialData::name)
    // Here we align the cell to the right side
    simple("Quantity", right, TutorialData::quantity)
}
```

Let's apply it on following data:

```kotlin
    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    println(formatter.apply(data))
```

The output would be the following:

```
+--------+----------+
| Name   | Quantity |
+--------+----------+
| apple  |     10.0 |
| banana |      5.5 |
+--------+----------+
```

As for the second example, let's do some aggregation:

```kotlin
    // We need a state class to maintain the aggregation state
    class TutorialAggregator(var sum: Double = 0.0)

    val formatter = tableFormatter<TutorialData> {
        // Enables aggregation
        showAggregation=true
        
        simpleLabeled("Name", "TOTAL", TutorialData::name)

        // A stateful column is a little more complex
        stateful<Double, TutorialAggregator>("Quantity", right) {
            // Initialization of the aggregation state
            initState { TutorialAggregator() }

            // The extractor of a stateful column has an additional 
            // responsibility above returning the value: it has to 
            // maintain the aggregation state
            extractor { d, a ->
                a.sum += d.quantity
                d.quantity
            }

            // The aggregator produces the aggregated value
            aggregator { _, a -> a.sum }
        }
    }
```

Running it on the same input as in the previous example:

```kotlin
    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    println(formatter.apply(data))
```

would produce the following output:

```
+--------+----------+
| Name   | Quantity |
+--------+----------+
| apple  |     10.0 |
| banana |      5.5 |
+--------+----------+
| TOTAL  |     15.5 |
+--------+----------+
```

If you don't understand what's happening in the code above, don't worry! These examples are only for
demonstrating some features of the library. Check the [Wiki](https://bitbucket.org/balage42/ptt-kotlin/wiki/Home)
for detailed guide! 

