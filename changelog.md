# root project 'ptt-kotlin'

### Version 1.1.4 (2021-03-08 10:07:49.891163) 

#### Bugfixes

- Fixed simple definition for nullable extractors

### Version 1.1.1 (2020-11-11 10:17:37.2115367) 

#### Bugfixes

- Fixed buildscript

### Version 1.1.0 (2020-11-11 10:07:23.5003853) 

#### New features

- Adding semantic versioning
- Adding locale to DefaultDateTimeConverters
- Doc for Converters
#### Bugfixes

- Refactoring to single project

