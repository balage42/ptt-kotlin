package hu.vissy.texttable.dsl

import hu.vissy.texttable.AggregatorRow
import hu.vissy.texttable.DataRow
import hu.vissy.texttable.InputRow
import hu.vissy.texttable.SeparatorRow

fun <D : Any> tableInput(op: InputBuilderKotlin<D>.() -> Unit) = InputBuilderKotlin<D>().apply(op).build()

@Suppress("unused")
class InputBuilderKotlin<D : Any> {
    val data = mutableListOf<InputRow<D>>()

    fun dataSet(rows: Collection<D>) {
        rows.forEach { data(it) }
    }

    fun dataSet(vararg rows: D) {
        rows.forEach { data(it) }
    }

    fun data(row: D?) {
        data += if (row == null) SeparatorRow<D>() else DataRow(row)
    }

    operator fun D.unaryPlus() {
        data(this)
    }

    fun sort( comparator: Comparator<D> ) {
        data.sortWith(Comparator { lr, rr ->
            if (lr !is DataRow<D> || rr !is DataRow<D>) 0
            else comparator.compare(lr.data, rr.data)
        })
    }

    fun separator() {
        data += SeparatorRow()
    }

    fun autoSeparator(predicate: (D, D) -> Boolean) {
        autoInserter(predicate) { i ->
            data.add(i, SeparatorRow())
            true
        }
    }

    fun aggregator(key: Any? = null) {
        data += AggregatorRow(key)
    }

    fun autoAggregator(key: Any, predicate: (D, D) -> Boolean) {
        autoInserter(predicate, true) { i ->
            data.add(i, AggregatorRow(key))
            false
        }
    }

    private fun autoInserter(predicate: (D, D) -> Boolean, addLast :Boolean = false, inserter: (Int) -> Boolean) {
        var prev: D? = null
        var i = 0
        while (i < data.size) {
            val d = data[i]
            if (d is DataRow) {
                if (prev != null) {
                    if (predicate(prev!!, d.data)) {
                        if (inserter(i)) i++
                    }
                }
                prev = d.data
            }
            i++
        }
        if(addLast) {
            val li = data.findLast { it is DataRow<D> }
            if (li != null) inserter(data.lastIndexOf(li) + 1)
        }
    }

    fun build(): List<InputRow<D>> = data.toList()
}