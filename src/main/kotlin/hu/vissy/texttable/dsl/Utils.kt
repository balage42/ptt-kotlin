package hu.vissy.texttable.dsl

fun <T> same(vararg items: T?): Boolean {
    (1 until items.size).forEach {
        if (items[it] != items[it - 1]) return false
    }
    return true
}
