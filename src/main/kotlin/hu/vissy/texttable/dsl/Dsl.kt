@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package hu.vissy.texttable.dsl

import hu.vissy.texttable.BorderFormatter
import hu.vissy.texttable.BorderFormatter.LineType
import hu.vissy.texttable.BorderFormatter.RowType
import hu.vissy.texttable.TableFormatter
import hu.vissy.texttable.column.ColumnDefinition
import hu.vissy.texttable.column.ColumnDefinition.StatefulBuilder
import hu.vissy.texttable.contentformatter.*
import hu.vissy.texttable.contentformatter.EllipsisDecorator.TextSegment
import hu.vissy.texttable.dataconverter.*
import hu.vissy.texttable.dataextractor.StatefulDataExtractor
import java.math.RoundingMode
import java.text.NumberFormat
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.temporal.Temporal
import java.util.*
import kotlin.properties.Delegates
import kotlin.reflect.KClass

@DslMarker
annotation class TableFormatterDsl

/**
 * Entry point for Table Formatter Configuration DSL.
 *
 * It creates a new [TableFormatterBuilder] instance and applies the [op] configuration on it,
 * then builds the formatter object.
 *
 * [D] is the type of the input record. A record represents one row of the table.
 *
 */
@TableFormatterDsl
fun <D : Any> tableFormatter(border: PredefinedBorder = SimpleAsciiBorder, op: TableFormatterBuilder<D>.() -> Unit) =
    TableFormatterBuilder<D>(border).apply(op).build()

class CounterState(var counter: Int = 0)


class NumberState<T : Number>(var value: T, var counter: Int = 0)

@TableFormatterDsl
class TableFormatterBuilder<D : Any>(predefined: PredefinedBorder) {
    private val columns = mutableListOf<ColumnDefinition<D, *, *>>()
    var heading: String? = null
    var showAggregation = false
    var separateDataWithLines = false
    private val border = BorderBuilder(predefined)

    fun build(): TableFormatter<D> {
        return TableFormatter.Builder<D>().apply {
            withBorderFormatter(border.build())
            withHeading(heading)
            withShowAggregation(showAggregation)
            withSeparateDataWithLines(separateDataWithLines)
            columns.forEach { withColumn(it) }
        }.build()
    }

    fun _internal_add(c: ColumnDefinition<D, *, *>) {
        columns += c
    }

    inline fun <reified T : Any> simple(title: String = "", noinline extractor: (D) -> T?) =
        simple(title, left, extractor = extractor)

    inline fun <reified T : Any> simple(
        title: String = "",
        alignment: CellAlignmentMarker = left,
        noinline extractor: (D) -> T?
    ) =
        StatelessColumnBuilder<D, T>(title, alignment, clazz = T::class).apply { this.extractor = extractor }.build()
            .apply { _internal_add(this) }

    inline fun <reified T : Any> stateless(
        title: String = "",
        alignment: CellAlignmentMarker = left,
        op: StatelessColumnBuilder<D, T>.() -> Unit
    ) =
        StatelessColumnBuilder<D, T>(title, alignment, T::class).apply(op).build().apply { _internal_add(this) }

    inline fun <reified T : Any> simpleLabeled(
        title: String = "",
        label: String = "",
        alignment: CellAlignmentMarker = left,
        noinline extractor: (D) -> T?
    ) =
        LabeledColumnBuilder<D, T>(title, label, alignment, T::class).apply { this.extractor = extractor }.build()
            .apply { _internal_add(this) }

    inline fun <reified T : Any> simpleLabeled(title: String = "", label: String = "", noinline extractor: (D) -> T?) =
        simpleLabeled(title, label, left, extractor)

    inline fun <reified T : Any> labeled(
        title: String = "",
        label: String = "",
        alignment: CellAlignmentMarker = left,
        op: LabeledColumnBuilder<D, T>.() -> Unit
    ) =
        LabeledColumnBuilder<D, T>(title, label, alignment, T::class).apply(op).build().apply { _internal_add(this) }

    inline fun <reified T : Any, S : Any> stateful(
        title: String = "",
        alignment: CellAlignmentMarker = left,
        op: StatefulColumnBuilder<D, S, T>.() -> Unit
    ) =
        StatefulColumnBuilder<D, S, T>(title, alignment, T::class).apply(op).build().apply { _internal_add(this) }

    fun index(
        title: String = "#",
        alignment: CellAlignmentMarker = right,
        op: StatefulColumnBuilder<D, CounterState, Int>.() -> Unit = {}
    ) {
        columns += StatefulColumnBuilder<D, CounterState, Int>(title, alignment, Int::class).apply {
            initState { CounterState() }
            //  converter<DefaultInt> {}
            extractor { _, counter -> ++counter.counter }
        }.apply(op).build()
    }

    fun border(predefined: PredefinedBorder? = SimpleAsciiBorder, op: BorderBuilder.() -> Unit = {}) =
        border.apply { if (predefined != null) predefined(predefined) }.apply(op)

}

private fun <T : Any> selectDataConverter(clazz: KClass<T>?) = when (clazz?.java) {
    Int::class.java -> DefaultInt().build()
    Long::class.java -> DefaultLong().build()
    String::class.java -> DefaultString().build()
    Double::class.java -> DefaultDouble().build()
    Boolean::class.java -> DefaultBoolean().build()
    LocalDate::class.java -> DefaultDate().build()
    LocalTime::class.java -> DefaultTime().build()
    LocalDateTime::class.java -> DefaultDateTime().build()
    Duration::class.java -> DefaultDuration().build()
    null -> TrivialDataConverter()
    else -> TypedTrivialDataConverter(clazz.java)
} as DataConverter<T?>


@TableFormatterDsl
sealed class StatelessColumnBuilderBase<D : Any, T : Any>(
    var title: String = "",
    alignment: CellAlignmentMarker = left,
    clazz: KClass<T>? = null
) {
    var extractor: ((D) -> T?)? = null
    var converter: DataConverter<T?> = selectDataConverter(clazz)


    var cellFormatter: CellContentFormatter = CellContentFormatterBuilder(alignment).build()

    fun extractor(f: (D) -> T?) {
        this.extractor = f
    }

    fun converter(op: (T?) -> String?) {
        this.converter = DataConverter<T?> { data -> op(data) }
    }

    inline fun <reified C : DataConverterBuilder<T?>> converter(op: C.() -> Unit = {}) =
        C::class.java.getDeclaredConstructor().newInstance().apply(op).build().apply { converter = this }

    fun cellFormatter(alignment: CellAlignmentMarker = left, op: CellContentFormatterBuilder.() -> Unit = {}) =
        CellContentFormatterBuilder(alignment).apply(op).build().apply { cellFormatter = this }


    abstract fun build(): ColumnDefinition<D, out Any, T>
}

@TableFormatterDsl
class StatelessColumnBuilder<D : Any, T : Any>(
    title: String = "",
    alignment: CellAlignmentMarker = left,
    clazz: KClass<T>
) :
    StatelessColumnBuilderBase<D, T>(title, alignment, clazz) {

    override fun build(): ColumnDefinition<D, Void, T> = ColumnDefinition.StatelessBuilder<D, T>()
        .withTitle(title)
        .withDataExtractor(extractor ?: throw IllegalStateException("Extractor must be specified"))
        .withDataConverter(converter)
        .withCellContentFormatter(cellFormatter)
        .build()
}


@TableFormatterDsl
class LabeledColumnBuilder<D : Any, T : Any>(
    title: String = "",
    label: String? = null,
    alignment: CellAlignmentMarker = left,
    clazz: KClass<T>
) : StatelessColumnBuilderBase<D, T>(title, alignment, clazz) {

    private val aggregateRowConstant = mutableMapOf<Any?, String>()

    init {
        if (label != null) aggregateRowConstant(label)
    }

    fun aggregateRowConstant(key: Any?, value: String = "") {
        aggregateRowConstant[key] = value
    }

    fun aggregateRowConstant(value: String = "") = aggregateRowConstant(null, value)

    override fun build(): ColumnDefinition<D, NoState, T> = StatefulBuilder<D, NoState, T>()
        .withTitle(title)
        .withDataExtractor(StatefulDataExtractor({ d, _ -> extractor?.invoke(d) },
            { NoState() }, { _, _ -> null })
        )
        .withDataConverter(converter)
        .apply {
            aggregateRowConstant.forEach { (k, v) ->
                this.withAggregateRowConstant(k, v)
            }
        }
        .withCellContentFormatter(cellFormatter)
        .build()
}


@TableFormatterDsl
class NoState {}


@TableFormatterDsl
open class StatefulColumnBuilder<D : Any, S : Any, T : Any>(
    var title: String = "",
    alignment: CellAlignmentMarker = left,
    clazz: KClass<T>?
) {
    var initState: (() -> S)? = null
    var extractor: ((D, S) -> T?)? = null
    var aggregator: (Any?, S) -> T? = { _, _ -> null }
    var converter: DataConverter<T?> = selectDataConverter(clazz)
    var cellFormatter: CellContentFormatter = CellContentFormatterBuilder(alignment).build()
    private val aggregateRowConstant = mutableMapOf<Any?, String>()

    fun aggregateRowConstant(key: Any?, value: String = "") {
        aggregateRowConstant[key] = value
    }

    fun aggregateRowConstant(value: String = "") = aggregateRowConstant(null, value)

    fun extractor(f: (D, S) -> T?) {
        this.extractor = f
    }

    fun initState(f: () -> S) {
        initState = f
    }

    fun aggregator(f: (Any?, S) -> T?) {
        aggregator = f
    }

    inline fun <reified C : DataConverterBuilder<T?>> converter(op: C.() -> Unit = {}) =
        C::class.java.getDeclaredConstructor().newInstance().apply(op).build().apply { converter = this }


    fun cellFormatter(alignment: CellAlignmentMarker = left, op: CellContentFormatterBuilder.() -> Unit = {}) =
        CellContentFormatterBuilder(alignment).apply(op).build().apply { cellFormatter = this }

    fun build(): ColumnDefinition<D, S, T> = StatefulBuilder<D, S, T>()
        .withTitle(title)
        .withDataExtractor(
            StatefulDataExtractor(
                extractor ?: throw IllegalStateException("Extractor must be specified"),
                initState ?: error("initState should be provided ($title)"),
                aggregator
            )
        )
        .withDataConverter(converter)
        .apply {
            aggregateRowConstant.forEach { (k, v) ->
                this.withAggregateRowConstant(k, v)
            }
        }
        .withCellContentFormatter(cellFormatter)
        .build()
}


@TableFormatterDsl
abstract class DataConverterBuilder<T> {
    abstract fun build(): DataConverter<T>
}

@TableFormatterDsl
abstract class DefaultDateTimeBase<T : Temporal>(default: String) : DataConverterBuilder<T?>() {
    var format: String = default
        set(value) {
            field = value
            formatter = DateTimeFormatter.ofPattern(value, locale)
        }
    var locale: Locale = Locale.getDefault()
        set(value) {
            field = value
            formatter = DateTimeFormatter.ofPattern(format, locale)
        }

    var formatter: DateTimeFormatter = DateTimeFormatter.ofPattern(format, locale)
}


@TableFormatterDsl
open class DefaultDate : DefaultDateTimeBase<LocalDate>("yyyy-MM-dd") {
    override fun build() = DateDataConverter(formatter)
}


@TableFormatterDsl
open class DefaultTime : DefaultDateTimeBase<LocalTime>("hh:mm:ss") {
    override fun build() = TimeDataConverter(formatter)
}


@TableFormatterDsl
open class DefaultDateTime : DefaultDateTimeBase<LocalDateTime>("yyyy-MM-dd hh:mm:ss") {
    override fun build() = DateTimeDataConverter(formatter)
}


@TableFormatterDsl
open class DefaultBoolean : DataConverterBuilder<Boolean?>() {
    var trueValue = "true"
    var falseValue = "false"
    override fun build() = BooleanDataConverter(trueValue, falseValue)
}

@TableFormatterDsl
enum class StringTransform(val converter: (String) -> String) {
    NONE({ it }),
    UPPER_CASE({ it.toUpperCase() }),
    LOWER_CASE({ it.toLowerCase() }),
    ;

}


inline fun <reified T : Any?> typedConverter(noinline conv: (T) -> String): DataConverter<T> =
    object : TypedDataConverter<T>(T::class.java) {
        override fun convert(data: T) = conv(data)
    }


@TableFormatterDsl
open class DefaultString : DataConverterBuilder<String?>() {
    var transform: StringTransform = StringTransform.NONE

    override fun build() = typedConverter<String?> { s ->
        if (s.isNullOrBlank()) "" else transform.converter(s)
    }
}


@TableFormatterDsl
abstract class DefaultNumber<T : Number>(val clazz: Class<T>) : DataConverterBuilder<T?>() {
    var formatter: NumberFormat = NumberFormat.getInstance()

    var locale: Locale = Locale.getDefault()
        set(value) {
            val old = formatter
            formatter = NumberFormat.getInstance(value)
            formatter.roundingMode = old.roundingMode
            formatter.isGroupingUsed = old.isGroupingUsed
            formatter.minimumFractionDigits = old.minimumFractionDigits
            formatter.maximumFractionDigits = old.maximumFractionDigits
        }

    var maximumFractionDigits
        get() = formatter.maximumFractionDigits
        set(value) {
            formatter.maximumFractionDigits = value
        }

    var minimumFractionDigits
        get() = formatter.minimumFractionDigits
        set(value) {
            formatter.minimumFractionDigits = value
        }

    var grouping
        get() = formatter.isGroupingUsed
        set(value) {
            formatter.isGroupingUsed = value
        }

    var rounding: RoundingMode
        get() = formatter.roundingMode
        set(value) {
            formatter.roundingMode = value
        }

    override fun build() = NumberDataConverter<T>(clazz, formatter)
}

@TableFormatterDsl
abstract class DefaultDecimal<T : Number>(clazz: Class<T>) : DefaultNumber<T>(clazz) {
    init {
        maximumFractionDigits = 0
        minimumFractionDigits = 0
        rounding = RoundingMode.UNNECESSARY
    }
}


@TableFormatterDsl
open class DefaultInt : DefaultDecimal<Int>(Int::class.java)

@TableFormatterDsl
open class DefaultLong : DefaultDecimal<Long>(Long::class.java)

@TableFormatterDsl
open class DefaultDouble : DefaultNumber<Double>(Double::class.java) {
    init {
        maximumFractionDigits = 2
        minimumFractionDigits = 2
        rounding = RoundingMode.HALF_UP
    }
}


@TableFormatterDsl
class DefaultDuration : DataConverterBuilder<Duration?>() {
    override fun build(): DataConverter<Duration?> =
        SimpleDurationDataConverter()
}

@TableFormatterDsl
sealed class CellAlignmentMarker {
    internal abstract fun build(padding: Char): CellAlignment
}

@Suppress("ClassName")
object left : CellAlignmentMarker() {
    override fun build(padding: Char) = LeftCellAlignment(padding)
}

@Suppress("ClassName")
object right : CellAlignmentMarker() {
    override fun build(padding: Char) = RightCellAlignment(padding)
}

@Suppress("ClassName")
object center : CellAlignmentMarker() {
    override fun build(padding: Char) = CenterCellAlignment(padding)
}


@TableFormatterDsl
class CellContentFormatterBuilder(alignment: CellAlignmentMarker = left) {
    private var cellAlignment: CellAlignment

    var nullValue: String = ""

    var minWidth: Int = 0
    var maxWidth: Int = Int.MAX_VALUE

    var fixedWidth: Int
        get() = if (minWidth == maxWidth) minWidth else -1
        set(value) {
            minWidth = value
            maxWidth = value
        }

    var ellipsis: EllipsisDecorator = EllipsisDecorator.Builder().build()

    var padding: Char = ' '
        set(value) {
            field = value
            cellAlignment = alignment.build(value)
        }

    var alignment: CellAlignmentMarker = alignment
        set(value) {
            field = value
            cellAlignment = value.build(padding)
        }

    init {
        cellAlignment = alignment.build(padding)
    }

    fun ellipsis(
        keep: TextSegmentMarker = segmentStart,
        sign: String = "...",
        op: EllipsisDecoratorBuilder.() -> Unit = {}
    ) = EllipsisDecoratorBuilder(keep, sign).apply(op).build().apply { ellipsis = this }


    fun build(): CellContentFormatter = CellContentFormatter.Builder()
        .withCellAlignment(cellAlignment)
        .withNullValue(nullValue)
        .withMinWidth(minWidth)
        .withMaxWidth(maxWidth)
        .withEllipsesDecorator(ellipsis)
        .build()
}


@TableFormatterDsl
sealed class TextSegmentMarker(val segment: TextSegment)

@Suppress("ClassName")
object segmentStart : TextSegmentMarker(TextSegment.START)

@Suppress("ClassName")
object segmentEnd : TextSegmentMarker(TextSegment.END)

@Suppress("ClassName")
object segmentCenter : TextSegmentMarker(TextSegment.CENTER)


@TableFormatterDsl
class EllipsisDecoratorBuilder(var keep: TextSegmentMarker = segmentStart, var sign: String = "...") {
    var trimToWord = false

    fun build(): EllipsisDecorator = EllipsisDecorator.Builder()
        .withEllipsisSign(sign)
        .withKeptPart(keep.segment)
        .withTrimToWord(trimToWord)
        .build()

}

@TableFormatterDsl
object PttTemplate {
    fun cellFormatter(alignment: CellAlignmentMarker = left, op: CellContentFormatterBuilder.() -> Unit = {}) =
        CellContentFormatterBuilder(alignment).apply(op).build()

    @JvmName("simpleConverter")
    fun <T : Any> converter(op: (T?) -> String?): DataConverter<T?> = DataConverter<T?> { data -> op(data) }

    inline fun <T : Any, reified C : DataConverterBuilder<T?>> converter(op: C.() -> Unit = {}): DataConverter<T?> =
        C::class.java.getDeclaredConstructor().newInstance().apply(op).build()

    fun ellipsis(
        keep: TextSegmentMarker = segmentStart,
        sign: String = "...",
        op: EllipsisDecoratorBuilder.() -> Unit = {}
    ) =
        EllipsisDecoratorBuilder(keep, sign).apply(op).build()
}


val topLine = LineType.TOP_EDGE
val headingLine = LineType.HEADING_LINE
val headerLine = LineType.HEADER_LINE
val internalLine = LineType.INTERNAL_LINE
val separatorLine = LineType.SEPARATOR_LINE
val aggregateLine = LineType.AGGREGATE_LINE
val bottomLine = LineType.BOTTOM_EDGE

val headingRow = RowType.HEADING
var headerRow = RowType.HEADER
var dataRow = RowType.DATA
var aggregateRow = RowType.AGGREGATE

@TableFormatterDsl
class BorderBuilder(predefined: PredefinedBorder? = null) {
    private var lines: MutableMap<LineType, LineSpecBuilder> =
        LineType.values().associate { it to LineSpecBuilder() }.toMutableMap()
    private var rows: MutableMap<RowType, RowSpecBuilder> =
        RowType.values().associate { it to RowSpecBuilder() }.toMutableMap()
    var drawVerticalEdge = true
    var drawVerticalSeparator = true

    var leftPadding: Int = 1
        set(value) {
            field = if (value >= 0) value else field
        }
    var rightPadding: Int = 1
        set(value) {
            field = if (value >= 0) value else field
        }

    init {
        predefined?.populate(this)
    }

    fun predefined(predefined: PredefinedBorder) {
        predefined.populate(this)
    }

    fun padding(left: Int, right: Int) {
        leftPadding = left
        rightPadding = right
    }

    fun padding(padding: Int) = padding(padding, padding)
    fun noPadding() = padding(0)

    fun row(vararg rowTypes: RowType = RowType.values(), op: RowSpecBuilder.() -> Unit) {
        rowTypes.forEach {
            rows.getValue(it).apply(op)
        }
    }

    fun line(vararg lineTypes: LineType = LineType.values(), op: LineSpecBuilder.() -> Unit) {
        lineTypes.forEach {
            lines.getValue(it).apply(op)
        }
    }

    fun build(): BorderFormatter {
        val bfb = BorderFormatter.Builder(BorderFormatter.DefaultFormatters.EMPTY)
        for (lineDef in lines) {
            bfb.withLine(lineDef.value.build(), lineDef.key)
        }
        for (rowDef in rows) {
            bfb.withRow(rowDef.value.build(), rowDef.key)
        }
        return bfb
            .withDrawVerticalEdge(drawVerticalEdge)
            .withDrawVerticalSeparator(drawVerticalSeparator)
            .withLeftPaddingWidth(leftPadding)
            .withRightPaddingWidth(rightPadding)
            .build()
    }
}


@TableFormatterDsl
abstract class SpecBuilder<T : BorderFormatter.LineSpec> {
    protected fun resetHiddenDelegate(init: Char = ' ') =
        Delegates.observable(init) { _, _, _ -> hidden = false }

    var left: Char by resetHiddenDelegate()
    var internal: Char by resetHiddenDelegate()
    var right: Char by resetHiddenDelegate()
    var padding: Char by resetHiddenDelegate()

    protected open var hidden = false

    var edge: Char
        get() = if (same(left, right)) left else mixed
        set(value) {
            left = value
            right = value
        }

    var vertical: Char
        get() = if (same(left, right, internal)) left else mixed
        set(value) {
            edge = value
            internal = value
        }

    abstract fun build(): T
}


@TableFormatterDsl
class RowSpecBuilder : SpecBuilder<BorderFormatter.RowSpec>() {
    override fun build(): BorderFormatter.RowSpec = BorderFormatter.RowSpec(left, internal, padding, right)
}

@TableFormatterDsl
class LineSpecBuilder : SpecBuilder<BorderFormatter.LineSpec>() {
    var body: Char by resetHiddenDelegate()

    public override var hidden = false

    var content: Char
        get() = if (same(padding, body)) padding else mixed
        set(value) {
            padding = value
            body = value
        }

    override fun build(): BorderFormatter.LineSpec =
        if (hidden) BorderFormatter.HIDDEN
        else BorderFormatter.LineSpec(left, internal, padding, body, right)
}


abstract class PredefinedBorder {
    abstract fun populate(builder: BorderBuilder)
}

const val empty = 0.toChar()
const val mixed = 1.toChar()

object EmptyBorder : PredefinedBorder() {
    override fun populate(builder: BorderBuilder) {
        with(builder) {
            drawVerticalEdge = false
            drawVerticalSeparator = false
            noPadding()
            line {
                hidden = true
            }
            row {
                vertical = empty
                padding = empty
            }
        }
    }
}

object SimpleAsciiBorder : PredefinedBorder() {
    override fun populate(builder: BorderBuilder) {
        with(builder) {
            line {
                vertical = '+'
                content = '-'
            }
            row {
                vertical = '|'
            }
        }
    }
}

object DoubleAsciiBorder : PredefinedBorder() {
    override fun populate(builder: BorderBuilder) {
        with(builder) {
            line {
                vertical = '+'
                content = '='
            }
            line(internalLine) {
                content = '-'
            }
            row {
                vertical = '|'
            }
        }
    }
}

object NoVerticalBorder : PredefinedBorder() {
    override fun populate(builder: BorderBuilder) {
        with(builder) {
            drawVerticalEdge = false
            line {
                vertical = '-'
                content = '-'
            }
            line(headerLine, separatorLine, aggregateLine) {
                edge = '-'
                internal = ' '
            }
            line(internalLine) {
                hidden = true
            }
            row {
                edge = ' '
            }
        }
    }
}


object UnicodeBorder : PredefinedBorder() {
    override fun populate(builder: BorderBuilder) {
        with(builder) {
            line(topLine) {
                left = '╔'
                internal = '╤'
                content = '═'
                right = '╗'
            }
            line(headerLine, headingLine, separatorLine, aggregateLine) {
                left = '╠'
                internal = '╪'
                content = '═'
                right = '╣'
            }
            line(internalLine) {
                left = '╟'
                internal = '┼'
                content = '─'
                right = '╢'
            }
            line(bottomLine) {
                left = '╚'
                internal = '╧'
                content = '═'
                right = '╝'
            }

            row {
                edge = '║'
                internal = '│'
            }
        }
    }
}


