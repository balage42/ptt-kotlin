package hu.vissy.texttable.kotlin.wiki

import hu.vissy.texttable.dsl.*
import java.math.RoundingMode
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Month
import java.util.*


fun main() {
    example3_2_1()
    example3_2_2()
    example3_2_3()
    example3_2_4()
    example3_2_5()
    example3_2_6()
    example3_2_7()
}


fun example3_2_1() = printTest("3.2.1 Data converter (capitalize)") {
    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Name") {
            extractor(TutorialData::name)

            converter { if (it == null) "" else it[0].toUpperCase() + it.substring(1).toLowerCase() }
        }
    }

    val data = listOf(
            TutorialData(name = "apple"),
            TutorialData(name = "banana")
    )

    formatter.apply(data)
}


fun example3_2_2() = printTest("3.2.2 Distinct, sorted list data converter") {
    val formatter = tableFormatter<TutorialData> {
        simple("Name", TutorialData::name)

        // The type of the column could be anything, even a complex structure or a real class
        stateless<List<String>>("Flags") {
            extractor(TutorialData::flags)
            converter { l ->
                if (l == null || l.isEmpty()) ""
                else l.distinct().sorted().joinToString()
            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", flags = listOf("red", "has core", "tree")),
            TutorialData(name = "banana", flags = listOf("tree", "tropical", "tree"))
    )

    formatter.apply(data)
}


fun example3_2_3() = printTest("3.2.3 String converter") {
    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Name") {
            extractor(TutorialData::name)
            converter<DefaultString> {
                transform = StringTransform.UPPER_CASE
            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple"),
            TutorialData(name = "banana")
    )

    formatter.apply(data)
}

fun example3_2_4() = printTest("3.2.4 Boolean converter") {
    val formatter = tableFormatter<TutorialData> {
        simple("Name", TutorialData::name)

        stateless<Boolean>("Valid") {
            extractor(TutorialData::isValid)
            converter<DefaultBoolean> {
                trueValue = "yes"
                falseValue = "no"
            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", isValid = true),
            TutorialData(name = "banana", isValid = false)
    )

    formatter.apply(data)
}


fun example3_2_5() = printTest("3.2.4 Int converters") {
    val formatter = tableFormatter<TutorialData> {
        // Use the settings from default Locale
        // The output may differ depending on the default
        stateless<Int>("Int (Default)", right) {
            extractor(TutorialData::length)
            converter<DefaultInt>()
        }

        // Use English locale settings
        stateless<Int>("Int (Eng)", right) {
            extractor(TutorialData::length)
            converter<DefaultInt> {
                locale = Locale.ENGLISH
            }
        }

        // Use Hungarian locale settings
        stateless<Int>("Int (Hun)", right) {
            extractor(TutorialData::length)
            converter<DefaultInt> {
                locale = Locale.forLanguageTag("HU")
            }
        }

        // Use English locale settings, without thousand grouping
        stateless<Int>("Int (no grouping)", right) {
            extractor(TutorialData::length)
            converter<DefaultInt> {
                locale = Locale.ENGLISH
                grouping = false
            }
        }

        // Use English locale settings, with fixed fractions
        // Note, that using fractions works on integers as well, although it will always be 0
        stateless<Int>("Int (fractions)", right) {
            extractor(TutorialData::length)
            converter<DefaultInt> {
                locale = Locale.ENGLISH
                minimumFractionDigits=4
            }
        }
    }

    val data = listOf(
            TutorialData(length = 1),
            TutorialData(length = 1551),
            TutorialData(length = 123456789),
    )

    formatter.apply(data)
}


fun example3_2_6() = printTest("3.2.4 Double converters") {
    val formatter = tableFormatter<TutorialData> {
        // Use the settings from default Locale
        // The output may differ depending on the default
        stateless<Double>("Double (Default)", right) {
            extractor(TutorialData::quantity)
            converter<DefaultDouble>()
        }

        // Use English locale settings
        stateless<Double>("Double (Eng)", right) {
            extractor(TutorialData::quantity)
            converter<DefaultDouble> {
                locale = Locale.ENGLISH
            }
        }

        // Use Hungarian locale settings
        stateless<Double>("Double (Hun)", right) {
            extractor(TutorialData::quantity)
            converter<DefaultDouble> {
                locale = Locale.forLanguageTag("HU")
            }
        }

        // Use English locale settings, without thousand grouping
        stateless<Double>("Double (no grouping)", right) {
            extractor(TutorialData::quantity)
            converter<DefaultDouble> {
                locale = Locale.ENGLISH
                grouping = false
            }
        }

        // Use English locale settings, with fixed fractions
        stateless<Double>("Double (fractions)", right) {
            extractor(TutorialData::quantity)
            converter<DefaultDouble> {
                locale = Locale.ENGLISH
                minimumFractionDigits=3
                maximumFractionDigits=3
            }
        }

        // Use English locale settings, with different rounding
        stateless<Double>("Double (rounding)", right) {
            extractor(TutorialData::quantity)
            converter<DefaultDouble> {
                locale = Locale.ENGLISH
                minimumFractionDigits=3
                maximumFractionDigits=3
                rounding = RoundingMode.FLOOR
            }
        }

    }

    val data = listOf(
            TutorialData(quantity = 10.0),
            TutorialData(quantity = -1551.0),
            TutorialData(quantity = 0.25),
            TutorialData(quantity = 3.1415),
            TutorialData(quantity = 1234567.8910),
    )

    formatter.apply(data)
}



fun example3_2_7() = printTest("3.2.7 Date/time converters") {
    val formatter = tableFormatter<TutorialData> {
        // Use the settings from default Locale
        stateless<LocalDateTime>("Default") {
            extractor(TutorialData::date)
            converter<DefaultDateTime>()
        }

        // Use format (by default locale)
        stateless<LocalDateTime>("User defined (default)") {
            extractor(TutorialData::date)
            converter<DefaultDateTime> {
                format = "MMM dd. H:mm a"
            }
        }

        // Use format (English)
        stateless<LocalDateTime>("User defined (Eng)") {
            extractor(TutorialData::date)
            converter<DefaultDateTime> {
                format = "MMM dd. H:mm a"
                locale = Locale.ENGLISH
            }
        }

        // Use format (Date) in English
        stateless<LocalDate>("Date") {
            extractor { d -> d.date.toLocalDate()}
            converter<DefaultDate> {
                format = "MMM dd."
                locale = Locale.ENGLISH
            }
        }
    }

    val data = listOf(
            TutorialData(date = LocalDateTime.now()),
            TutorialData(date = LocalDateTime.of(2020, Month.SEPTEMBER, 1, 5, 40, 30)),
    )

    formatter.apply(data)
}
