package hu.vissy.texttable.kotlin.wiki

import hu.vissy.texttable.dsl.PttTemplate
import hu.vissy.texttable.dsl.right
import hu.vissy.texttable.dsl.segmentEnd
import hu.vissy.texttable.dsl.tableFormatter


fun main() {
    example4_1_1()
    example4_1_2()
    example4_1_3()
}


fun example4_1_1() = printTest("4.1.1 Reuse definitions") {
    // A special converter to add parenthesis to the value
    val addParenthesis = PttTemplate.converter<String> { s -> if (s == null) null else "($s)" }

    // Special ellipsis
    val myEllipsis = PttTemplate.ellipsis(segmentEnd, "__")

    // Defines a right-aligned column formatter with a fixed width of 10
    val right = PttTemplate.cellFormatter {
        alignment = right
        fixedWidth = 10
        ellipsis = myEllipsis
    }

    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Long") {
            extractor(TutorialData::name)
            // using the template
            converter = addParenthesis
            cellFormatter = right
        }
        stateless<String>("Ellipse") {
            extractor(TutorialData::name)
            // using the template again
            converter = addParenthesis
            cellFormatter {
                maxWidth = 5
                ellipsis = myEllipsis
            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple"),
            TutorialData(name = "banana"),
            TutorialData(name = "purple-dragonfruit")
    )

    formatter.apply(data)
}


fun example4_1_2() = printTest("4.1.2 Index") {
    val formatter = tableFormatter<TutorialData> {
        index("#")
        simple("Name", TutorialData::name)
    }

    val data = listOf(
            TutorialData(name = "apple"),
            TutorialData(name = "banana")
    )

    formatter.apply(data)
}


fun example4_1_3() = printTest("4.1.3 Separator line") {
    val formatter = tableFormatter<TutorialData> {
        index("#")
        simple("Name", TutorialData::name)
    }

    val data = listOf(
            TutorialData(name = "apple"),
            TutorialData(name = "banana"),
            null,
            TutorialData(name = "cherry"),
            null,
            TutorialData(name = "date"),
    )

    formatter.apply(data)
}

