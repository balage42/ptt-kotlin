package hu.vissy.texttable.kotlin.wiki

import hu.vissy.texttable.dsl.NoState
import hu.vissy.texttable.dsl.right
import hu.vissy.texttable.dsl.tableFormatter


fun main() {
    example3_1()
    example3_2()
    example3_3()
    example3_4()
}


fun example3_1() = printTest("3.1 Simple aggregation") {
    class State(var sum: Double = 0.0)

    val formatter = tableFormatter<TutorialData> {
        heading = "Aggregation"
        showAggregation = true

        simple("Name", TutorialData::name)
        // Creating a stateful column
        stateful<Double, State>("Quantity", right) {
            // Initialization: creates a new State instance
            initState { State() }

            // Extract the value and update the state. Note that the extractor has two parameters.
            extractor { d, s ->
                s.sum += d.quantity
                d.quantity
            }

            // Provides the aggregated value
            aggregator { _, s -> s.sum }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    formatter.apply(data)
}


fun example3_2() = printTest("3.2 Static value for stateless columns in aggregation") {
    class State(var sum: Double = 0.0)

    val formatter = tableFormatter<TutorialData> {
        heading = "Aggregation"
        showAggregation = true

        // Faking the name column to be stateful
        stateful<String, NoState>("Name") {
            // NoState is a built-in utility class to pass as dummy state
            initState { NoState() }
            // The extractor do not need to update the dummy state, so acts almost like
            // a stateless extractor
            extractor { d, _ -> d.name }
            // The aggregator returns the label as a constant
            aggregator { _, _ -> "TOTAL" }
        }

        // Creating a stateful column
        stateful<Double, State>("Quantity", right) {
            // Initialization: creates a new State instance
            initState { State() }

            // Extract the value and update the state. Note that the extractor has two parameters.
            extractor { d, s ->
                s.sum += d.quantity
                d.quantity
            }

            // Provides the aggregated value
            aggregator { _, s -> s.sum }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    formatter.apply(data)
}

fun example3_3() = printTest("3.3 Static value for stateless columns in aggregation (using label)") {

    class State(var sum: Double = 0.0)

    val formatter = tableFormatter<TutorialData> {
        heading = "Aggregation"
        showAggregation = true


        // Using label would remove most of the boiler code
        labeled<String>("Name", "TOTAL") {
            extractor { d -> d.name }
        }

        // Or even this would work
        // simpleLabeled("Name", "TOTAL") { d -> d.name }

        // Creating a stateful column
        stateful<Double, State>("Quantity", right) {
            // Initialization: creates a new State instance
            initState { State() }

            // Extract the value and update the state. Note that the extractor has two parameters.
            extractor { d, s ->
                s.sum += d.quantity
                d.quantity
            }

            // Provides the aggregated value
            aggregator { _, s -> s.sum }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    formatter.apply(data)
}


fun example3_4() = printTest("3.4 Windowed calculation (difference)") {

    class State(var prev: Double? = null)

    val formatter = tableFormatter<TutorialData> {
        heading = "Export"

        simple("Period", TutorialData::name)
        simple("Quantity", right, TutorialData::quantity)

        // Creating a stateful column
        stateful<Double, State>("Change", right) {
            // Initialization: creates a new State instance
            initState { State() }

            // Extract the value and update the state. Note that the extractor has two parameters.
            extractor { d, s ->
                val v = if (s.prev == null) null
                else (d.quantity - s.prev!!)
                s.prev = d.quantity
                v
            }
        }
    }

    val data = listOf(
            TutorialData(name = "2020. Q1", quantity = 10.0),
            TutorialData(name = "2020. Q2", quantity = 5.5),
            TutorialData(name = "2020. Q3", quantity = 13.0),
            TutorialData(name = "2020. Q4", quantity = 13.0),
    )

    formatter.apply(data)
}

