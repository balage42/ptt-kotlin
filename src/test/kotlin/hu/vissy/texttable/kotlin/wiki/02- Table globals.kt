package hu.vissy.texttable.kotlin.wiki

import hu.vissy.texttable.dsl.*


fun main() {
    example2_1()
    example2_2()
    example2_3()
    example2_4()
}


fun example2_1() = printTest("2.1 Header") {
    val formatter = tableFormatter<TutorialData> {
        heading = "Fruit stocks"

        simple("Name", TutorialData::name)
        simple("Quantity", right, TutorialData::quantity)
    }

    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    formatter.apply(data)
}


fun example2_2() = printTest("2.2 Different, predefined border") {
    val formatter = tableFormatter<TutorialData> {
        heading = "Fruit stocks"
        border(DoubleAsciiBorder)

        simple("Name", TutorialData::name)
        simple("Quantity", right, TutorialData::quantity)
    }

    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    formatter.apply(data)
}

fun example2_3() = printTest("2.3 Predefined border") {
    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    val sb = StringBuilder()

    for (b in listOf(SimpleAsciiBorder, DoubleAsciiBorder, EmptyBorder, NoVerticalBorder, UnicodeBorder)) {
        sb.append("\nCLASS=${b::class.java.simpleName}\n")
        val formatter = tableFormatter<TutorialData>(b) {
            heading = "Fruit stocks"

            simple("Name", TutorialData::name)
            simple("Quantity", right, TutorialData::quantity)
        }

        sb.append(formatter.apply(data))
    }

    sb.toString()
}


fun example2_4() = printTest("2.4 User defined border") {
    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    val formatter = tableFormatter<TutorialData> {
        heading = "Fruit stocks"

        // We are starting from the predefined Double Ascii border
        border(DoubleAsciiBorder) {
            // Without selector, all lines are set
            line {
                padding = '#'
            }

            // Overrides the header line
            line(headerLine) {
                // Sets each part separately to demonstrate which is which
                left = '1'
                internal = '2'
                body = '3'
                padding = '4'
                right = '5'
            }

            line(bottomLine) {
                // Hides line
                hidden = true
            }

            // Overrides
            row {
                // Sets each padding to a dot
                padding = '.'
            }

            // Overrides the listed rows
            row(headingRow, headerRow) {
                // Sets the outer edges to an exclamation mark
                edge = '!'
            }
        }

        simple("Name", TutorialData::name)
        simple("Quantity", right, TutorialData::quantity)
    }

    formatter.apply(data)
}
