package hu.vissy.texttable.kotlin.wiki

import hu.vissy.texttable.dsl.tableFormatter
import hu.vissy.texttable.dsl.tableInput


fun main() {
    example4_2_1()
    example4_2_2()
    example4_2_3()
    example4_2_4()
    example4_2_5()
//    example4_2_6()
//    example4_2_7()
}


fun example4_2_1() = printTest("4.2.1 A simple example") {

    val formatter = tableFormatter<TutorialData> {
        index("#")
        simple("Name", TutorialData::name)
    }

    val data = tableInput<TutorialData> {
        // Add a single line by operator
        +TutorialData(name = "apple")

        // Add a single line by function
        data(TutorialData(name = "banana"))

        // Add a separator
        separator()

        // Add multiple lines as vararg
        dataSet(TutorialData(name = "cherry"), TutorialData(name = "date"))

        // Add multiple lines as collection
        dataSet(listOf(TutorialData(name = "eggplant"), TutorialData(name = "fig")))
    }

    formatter.applyToInput(data)
}

class ExportReport(val fruit: String, val period: String, val quantity: Double)
enum class ExportAggregationKeys { PER_FRUIT, TOTAL }

class ExportAggregationState(var sum: Double = 0.0, var total: Double = 0.0)

val formatter = tableFormatter<ExportReport> {
    index("#")
    labeled<String>("Fruit") {
        extractor(ExportReport::fruit)
        // We provide labels for each aggregator keys.
        aggregateRowConstant(ExportAggregationKeys.PER_FRUIT, "subtotal")
        aggregateRowConstant(ExportAggregationKeys.TOTAL, "TOTAL")
    }
    simple("Period", ExportReport::period)

    stateful<Double, ExportAggregationState>("Quantity") {
        initState { ExportAggregationState() }
        extractor { d, state ->
            val v = d.quantity
            // Updating both sums
            state.sum += v
            state.total += v
            v
        }
        aggregator { key, state ->
            // We use the key parameter to find out, which value should be
            // aggregated
            when (key as ExportAggregationKeys) {
                ExportAggregationKeys.PER_FRUIT -> {
                    val sum = state.sum
                    // Resetting the value after returned
                    state.sum = 0.0
                    sum
                }
                ExportAggregationKeys.TOTAL -> state.total
            }
        }
    }
}


fun example4_2_2() = printTest("4.2.2 Subtotals (manual)") {

    val data = tableInput<ExportReport> {
        +ExportReport("apple", "2019-Q1", 10.0)
        +ExportReport("apple", "2019-Q2", 5.0)
        +ExportReport("apple", "2019-Q3", 4.2)
        +ExportReport("apple", "2019-Q4", 14.0)
        aggregator(ExportAggregationKeys.PER_FRUIT)
        +ExportReport("banana", "2019-Q1", 20.0)
        +ExportReport("banana", "2019-Q2",  7.2)
        +ExportReport("banana", "2019-Q3",  5.4)
        +ExportReport("banana", "2019-Q4", 13.1)
        aggregator(ExportAggregationKeys.PER_FRUIT)
        aggregator(ExportAggregationKeys.TOTAL)
    }

    formatter.applyToInput(data)
}


fun example4_2_3() = printTest("4.2.3 Subtotals (automatic)") {

    val data = tableInput<ExportReport> {
        dataSet(
                ExportReport("apple", "2019-Q1", 10.0),
                ExportReport("apple", "2019-Q2", 5.0),
                ExportReport("apple", "2019-Q3", 4.2),
                ExportReport("apple", "2019-Q4", 14.0),
                ExportReport("banana", "2019-Q1", 20.0),
                ExportReport("banana", "2019-Q2", 7.2),
                ExportReport("banana", "2019-Q3", 5.4),
                ExportReport("banana", "2019-Q4", 13.1),
        )
        // Auto aggregator calls the provided predicates for each consecutive
        // data row pairs. If the predicate returns true, an aggregator is inserted
        // between them.
        autoAggregator(ExportAggregationKeys.PER_FRUIT) { l, r -> l.fruit != r.fruit}
        aggregator(ExportAggregationKeys.TOTAL)
    }

    formatter.applyToInput(data)
}

fun example4_2_4() = printTest("4.2.4 Automatic separators") {

    val data = tableInput<ExportReport> {
        dataSet(
                ExportReport("apple", "2019-Q1", 10.0),
                ExportReport("apple", "2019-Q2", 5.0),
                ExportReport("apple", "2019-Q3", 4.2),
                ExportReport("apple", "2019-Q4", 14.0),
                ExportReport("banana", "2019-Q1", 20.0),
                ExportReport("banana", "2019-Q2", 7.2),
                ExportReport("banana", "2019-Q3", 5.4),
                ExportReport("banana", "2019-Q4", 13.1),
        )

        autoSeparator { l, r -> l.fruit != r.fruit || r.period.endsWith("Q3")}
    }

    formatter.applyToInput(data)
}

fun example4_2_5() = printTest("4.2.5 Sorting") {

    val data = tableInput<ExportReport> {
        dataSet(
                ExportReport("apple", "2019-Q1", 10.0),
                ExportReport("banana", "2019-Q4", 13.1),
                ExportReport("banana", "2019-Q2", 7.2),
                ExportReport("apple", "2019-Q4", 14.0),
                ExportReport("banana", "2019-Q1", 20.0),
                ExportReport("apple", "2019-Q3", 4.2),
                ExportReport("banana", "2019-Q3", 5.4),
                ExportReport("apple", "2019-Q2", 5.0),
        )

        // Note: sort should be called before any special line is added
        sort( compareBy(ExportReport::fruit, ExportReport::period))

        // Adding separator lines between Q2 and Q3
        autoSeparator { _, r -> r.period.endsWith("Q3")}

        // Auto aggregator calls the provided predicates for each consecutive
        // data row pairs. If the predicate returns true, an aggregator is inserted
        // between them.
        autoAggregator(ExportAggregationKeys.PER_FRUIT) { l, r -> l.fruit != r.fruit}
        aggregator(ExportAggregationKeys.TOTAL)
    }

    formatter.applyToInput(data)
}
