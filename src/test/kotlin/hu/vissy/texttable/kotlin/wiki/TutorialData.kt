package hu.vissy.texttable.kotlin.wiki

import java.time.Duration
import java.time.LocalDateTime

data class TutorialData(
        val id: Long = 0,
        val name: String = "",
        val quantity: Double = 1.0,
        val date: LocalDateTime = LocalDateTime.now(),
        val duration: Duration = Duration.ZERO,
        val isValid: Boolean? = false,
        val length: Int = 0,
        val flags: List<String> = listOf())
