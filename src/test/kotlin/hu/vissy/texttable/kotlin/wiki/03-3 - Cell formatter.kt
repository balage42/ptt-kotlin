package hu.vissy.texttable.kotlin.wiki

import hu.vissy.texttable.dsl.*


fun main() {
    example3_3_1()
    example3_3_2()
    example3_3_3()
    example3_3_4()
    example3_3_5()
    example3_3_6()
    example3_3_7()
}


fun example3_3_1() = printTest("3.3.1 Column width") {
    val formatter = tableFormatter<TutorialData> {
        simple("Normal", TutorialData::name)
        simple("Long header overrides width", TutorialData::name)
        stateless<String>("Long") {
            extractor(TutorialData::name)
            cellFormatter {
                minWidth = 15
            }
        }
        stateless<String>("Short") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth = 5

            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", length = 42),
            TutorialData(name = "banana", length = 123456789)
    )

    formatter.apply(data)
}


fun example3_3_2() = printTest("3.3.2 Alignment") {
    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Left") {
            extractor(TutorialData::name)
            cellFormatter {
                minWidth = 10
                // This is the default, may be omitted
                alignment = left
            }
        }
        stateless<String>("Center") {
            extractor(TutorialData::name)
            cellFormatter {
                minWidth = 10
                alignment = center
            }
        }
        stateless<String>("Right") {
            extractor(TutorialData::name)
            cellFormatter {
                minWidth = 10
                alignment = right
            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple"),
            TutorialData(name = "banana")
    )

    formatter.apply(data)
}


fun example3_3_3() = printTest("3.3.3 Padding") {
    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Left") {
            extractor(TutorialData::name)
            cellFormatter {
                minWidth = 10
                padding='-'
            }
        }
        stateless<String>("Center") {
            extractor(TutorialData::name)
            cellFormatter {
                minWidth = 10
                alignment = center
                padding='.'
            }
        }
        stateless<String>("Right") {
            extractor(TutorialData::name)
            cellFormatter {
                minWidth = 10
                alignment = right
                padding='#'
            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple"),
            TutorialData(name = "banana")
    )

    formatter.apply(data)
}

fun example3_3_4() = printTest("3.3.4 Default ellipsis") {
    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Name") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=10
            }
        }
    }

    val data = listOf(
            TutorialData(name = "A longer text to display"),
    )

    formatter.apply(data)
}


fun example3_3_5() = printTest("3.3.4 Ellipsis (keep)") {
    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Start") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=15
            }
        }
        stateless<String>("Center") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=15
                ellipsis(segmentCenter)
            }
        }
        stateless<String>("End") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=15
                ellipsis(segmentEnd)
            }
        }
        stateless<String>("No sign") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=15
                ellipsis(segmentStart, "")
            }
        }
        stateless<String>("Different sign") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=15
                ellipsis(segmentStart, ">>")
            }
        }
    }

    val data = listOf(
            TutorialData(name = "A longer text to display"),
    )

    formatter.apply(data)
}

fun example3_3_6() = printTest("3.3.4 Ellipsis (word)") {
    val formatter = tableFormatter<TutorialData> {
        stateless<String>("Start") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=15
                ellipsis {
                    trimToWord= true
                }
            }
        }
        stateless<String>("End") {
            extractor(TutorialData::name)
            cellFormatter {
                maxWidth=15
                ellipsis(segmentEnd) {
                    trimToWord= true
                }
            }
        }
    }

    val data = listOf(
            TutorialData(name = "A longer text to display"),
    )

    formatter.apply(data)
}


fun example3_3_7() = printTest("3.3.7 Null values") {
    val formatter = tableFormatter<TutorialData> {
        simple("Fruit", TutorialData::name)
        stateless<Boolean>("In stock") {
            extractor(TutorialData::isValid)
            converter {
                when {
                    it == null -> null
                    it -> "yes"
                    else -> "no"
                }
            }
            cellFormatter {
                nullValue="unknown"
            }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", isValid = true),
            TutorialData(name = "banana", isValid = false),
            TutorialData(name = "cherry", isValid = null),
    )

    formatter.apply(data)
}

