package hu.vissy.texttable.kotlin.wiki

import hu.vissy.texttable.dsl.right
import hu.vissy.texttable.dsl.tableFormatter


fun main() {
    example1_1()
    example1_2()
    example1_3()
}

fun printTest(header: String, op: () -> String) {
    println("Example $header\n")
    println(op())
    println("\n\n")
}


fun example1_1() = printTest("1.1 The famous hello world") {
    val formatter = tableFormatter<TutorialData> {
        simple("Column") { d -> d.name }
    }

    val data = listOf(
            TutorialData(name = "Hello world")
    )

    formatter.apply(data)
}


fun example1_2() = printTest("1.2 A more complex example") {
    val formatter = tableFormatter<TutorialData> {
        // For one-to-one data fetching, the column reference
        // format may also be used
        simple("Name", TutorialData::name)
        simple("Quantity", right, TutorialData::quantity)
    }

    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    formatter.apply(data)
}


fun example1_3() = printTest("1.3 An aggregation example") {
    // We need a state class to maintain the aggregation state
    class TutorialAggregator(var sum: Double = 0.0)

    val formatter = tableFormatter<TutorialData> {
        // Enables aggregation
        showAggregation = true

        simple("Name", TutorialData::name)

        // A stateful column a little more complex
        stateful<Double, TutorialAggregator>("Quantity", right) {
            // Initialization of the aggregation state
            initState { TutorialAggregator() }

            // The extractor is responsible to maintain the aggregation state
            extractor { d, a ->
                a.sum += d.quantity
                d.quantity
            }

            // The aggregator produces the aggregation value
            aggregator { _, a -> a.sum }
        }
    }

    val data = listOf(
            TutorialData(name = "apple", quantity = 10.0),
            TutorialData(name = "banana", quantity = 5.5)
    )

    formatter.apply(data)
}
