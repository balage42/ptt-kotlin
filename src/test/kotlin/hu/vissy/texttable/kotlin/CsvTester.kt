package hu.vissy.texttable.kotlin

import hu.vissy.texttable.CsvTableFormatterBuilder
import hu.vissy.texttable.dsl.DoubleAsciiBorder
import hu.vissy.texttable.dsl.right
import hu.vissy.texttable.dsl.tableFormatter
import hu.vissy.texttable.dsl.typedConverter
import org.testng.annotations.Test

class CsvTester {

    @Test
    fun whenUsingIndex__theConversionWorks() {
        val formatter = tableFormatter<Int> {
            index()
            simple("Val") { t -> t }
        }

        val data = listOf(1, 4, 9, 16, 25)
        val csv = CsvTableFormatterBuilder<Int>().fromTableFormatter(formatter)
            .withDelimiter(';')
            .build().apply(data)
        println(csv)
    }


    class SolverStatistics(
        val workerId: String, val start: Int, val end: Int, val result: String, val error: String?, val ref: String,
        val firstSolutionObjectiveValue: Double? = null, val firstSolutionTime: Int? = null,
        val bestSolutionObjectiveValue: Double? = null, val bestSolutionTime: Int? = null,
        val bound: Double? = null
    ) {
        val duration = end - start
    }

    private fun formatDuration(time: Int) = "%d:%02d (%3d)".format(time / 60, time % 60, time)

    @Test
    fun test2() {
        val formatter = tableFormatter<SolverStatistics> {
            heading = "x"
            border(DoubleAsciiBorder)

            index("#")
            simple("Reference") { s -> s.ref }
            simple("Worker") { s -> s.workerId }
            simple("Start") { s -> s.start }
            simple("End") { s -> s.end }

            stateless<Int>("First time", right) {
                extractor { s -> s.firstSolutionTime }
                converter = typedConverter { v -> if (v != null) formatDuration(v) else "---" }
            }
            stateless<Int>("Best time", right) {
                extractor { s -> s.bestSolutionTime }
                converter = typedConverter { v -> if (v != null) formatDuration(v) else "---" }
            }
            stateless<Int>("Duration", right) {
                extractor { s -> s.duration }
                converter = typedConverter { v -> if (v != null) formatDuration(v) else "---" }
            }
            stateless<Double>("First", right) {
                extractor { s -> s.firstSolutionObjectiveValue }
                converter = typedConverter { v -> if (v != null) "%5.0f".format(v) else "---" }
            }
            stateless<Double>("Best", right) {
                extractor { s -> s.bestSolutionObjectiveValue }
                converter = typedConverter { v -> if (v != null) "%5.0f".format(v) else "---" }
            }
            stateless<Double>("Bound", right) {
                extractor { s -> s.bound }
                converter = typedConverter { v -> if (v != null) "%5.0f".format(v) else "---" }
            }

            simple("Result") { s -> s.result }
            simple("Error") { s -> s.error }
        }

        val csv = CsvTableFormatterBuilder<SolverStatistics>().fromTableFormatter(formatter)
            .withDelimiter(';')
            .build().apply(
                listOf(
                    SolverStatistics("X", 1, 2, "y", null, "xx", 0.2, null, 1.1, null, null)
                )
            )
        println(csv)
    }
}